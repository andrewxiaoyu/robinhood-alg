import time
from datetime import datetime

import requests
USE_TALIB = None
try:
    import talib
    USE_TALIB = True
except:
    import stockstats
    from stockstats import StockDataFrame
    import pandas as pd
    USE_TALIB = False
print("USE_TALIB...", USE_TALIB)

import numpy as np
from tqdm import tqdm


class Optimizers():

    stock_data = {}
    maxalpha_tickers = []

    RECENT_EVENTS_RANKS = {
        "Earnings": 3,
        "High Volume": 2
    }

    def maxalpha_ticker_rank(maxalpha_data):
        rank = 0
        news = maxalpha_data[12].split("<br>")
        if len(news) == 0:
            return 0

        for n in news:
            if len(n) == 0:
                continue
            if n.endswith("Press Releases") or n.endswith("Press Release"):
                rank += int(n.split()[0])
            elif n in Optimizers.RECENT_EVENTS_RANKS:
                rank += Optimizers.RECENT_EVENTS_RANKS[n]
            else:
                print("hmmm there's something wrong with the format of the maxalpha recent events")
                print("I got `{}`, but I have no idea what that is. Sorry :(".format(n))
        return rank

    def obtain_maxalpha_tickers(num_tries=0):
        try:
            r = requests.get("https://maxalpha.co/watch_list__data.php?list=watch_list")
            r = r.json()["data"]
            if len(r) == 0:
                raise Exception("XD Nothing there")
            r.sort(key=Optimizers.maxalpha_ticker_rank, reverse=True)
            Optimizers.maxalpha_tickers = [x[0] for x in r]
        except Exception as e:
            print("obtain_maxalpha_tickers failed, this was try number {}".format(num_tries+1))
            print("The error was: {}".format(str(e)))
            time.sleep(1)
            Optimizers.obtain_maxalpha_tickers(num_tries+1)
        return Optimizers.maxalpha_tickers

    """
    Booleans
    """
    def is_tradeable(stock_ticker):
        try:
            return requests.get("https://api.robinhood.com/instruments/?symbol={}".format(stock_ticker)).json()["results"][0]["tradeable"]
        except:
            return False

    """
    Obtains data from Yahoo finance
    """
    def get_premarket_timestamps():
        today = datetime.now()
        x=datetime(year=2018, month=today.month, day=today.day, hour=7, minute=0)
        y=datetime(year=2018, month=today.month, day=today.day, hour=9, minute=30)
        return int(x.timestamp()), int(y.timestamp())

    def obtain_data(stock_ticker, x, y):
        res = requests.get("https://query1.finance.yahoo.com/v8/finance/chart/{}?symbol={}".format(stock_ticker, stock_ticker) +
        "&period1={}&period2={}&interval=5m&includePrePost=true&lang=en-US&region=US".format(x,y)).json()

        res = res["chart"]["result"][0]
        if "timestamp" not in res:
            return False, []

        data_entry = 0
        num_intervals = int((y-x)/300)
        data = []

        for a in range( num_intervals ):
            current = x + (a * 300)

            if current in res["timestamp"]:
                entry_index = res["timestamp"].index(current)
                data_point = res["indicators"]["quote"][0]["close"][entry_index]
                if data_point:
                    data_entry = data_point

            data.append(data_entry)
        if len(data) > 0:
            return True, np.array(data, dtype=np.float)
        return False, []

    def plot_pktr(prices, pk, tr):
        plt.plot(tr, [prices[x] for x in tr], marker='o', color='b', ls='')
        plt.plot(pk, [prices[x] for x in pk], marker='o', color='y', ls='')
        plt.plot(prices)
        plt.show()

    def get_peaks_troughs(prices):
        tr = []
        pk = []
        for i in range(1, len(prices)-1):
            if prices[i-1] < prices[i] > prices[i+1]:
                pk.append(i)
            elif prices[i-1] > prices[i] < prices[i+1]:
                tr.append(i)
        return (pk, tr)

    def determine_trend(y):
        trend = np.poly1d(np.polyfit(range(len(y)), y, 1))(len(y))
        slope = np.polyfit(range(len(y)), y, 1)[0]
        if y[-1] < trend:
            return -1
        if y[-1] > trend:
            return 1
        return 0

    """
    Optimizers
    """
    def get_stop_loss(stock_data):
        std = np.std(stock_data)
        perc = (std/stock_data[-1]) * 100
        return perc

    def get_stockstats_df(stock_data):
        v = pd.DataFrame({"close": stock_data})
        return StockDataFrame(v)

    def RSI(stock_data):
        if USE_TALIB:
            real = talib.RSI(stock_data, timeperiod=14)
        else:
            real = np.array(Optimizers.get_stockstats_df(stock_data)["rsi_14"])
        real = [x for x in real if not np.isnan(x)]

        return Optimizers.determine_trend(real)

    def MACD(stock_data):
        if USE_TALIB:
            macd, macdsignal, macdhist = talib.MACD(stock_data, fastperiod=8, slowperiod=10, signalperiod=5)
        else:
            stockstats_df = Optimizers.get_stockstats_df(stock_data)
            macd = np.array(stockstats_df["macd"])
            macdsignal = np.array(stockstats_df["macds"])
            macdhist = np.array(stockstats_df["macdh"])

        macd = [x for x in macd if not np.isnan(x)]

        return Optimizers.determine_trend(macd)

    def CCI(ticker):
        close = [c['close'] for c in stock_data[ticker][-14:] ]
        high = [c['high'] for c in stock_data[ticker][-14:] ]
        low = [c['low'] for c in stock_data[ticker][-14:] ]
        if USE_TALIB:
            real = talib.CCI(high, low, close, timeperiod=14)
        else:
            real = np.array(Optimizers.get_stockstats_df(stock_data)["cci"])

        if real > 100:
            return 1
        elif real < -100:
            return -1
        return 0

    def AccumDist(ticker):
        volume = [c['volume'] for c in stock_data[ticker][-14:] ]
        close = [c['close'] for c in stock_data[ticker][-14:] ]
        high = [c['high'] for c in stock_data[ticker][-14:] ]
        low = [c['low'] for c in stock_data[ticker][-14:] ]
        real = MFI(high, low, close, volume, timeperiod=14)

        # TODO I don't know if we want to calculate the Accum/Dist ourselves, talib doesn't provide it

        vol = Volume(ticker)
        if vol * real > 0:
            return 1
        elif vol * real < 0:
            return -1
        return 0

    def Volume(ticker):
        volume = [c['volume'] for c in stock_data[ticker][-14:] ]

        # # TODO: possibly check for std dev
        if volume[-1] > volume[0]:
            return 1
        elif volume[-1] < volume[0]:
            return -1
        return 0

    def MaxAlpha(ticker):
        return ticker.upper() in Optimizers.maxalpha_tickers
