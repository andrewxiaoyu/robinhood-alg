import requests
import time

class MaxAlphaCrawler():

    def obtain_stocks(self):
        t = int(time.time()*1000)-500
        r = requests.get("https://maxalpha.co/watch_list__data.php?list=watch_list&session=2&_={}".format(t))
        return [t[0] for t in r.json()["data"]]
