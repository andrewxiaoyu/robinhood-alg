from robinhood_trader import RobinhoodTrader
from max_alpha import MaxAlphaCrawler
from optimizers import Optimizers
import pandas_market_calendars as mcal

import datetime
import threading
import time
import pickle
import copy
import traceback
from tqdm import tqdm

"""
Helpers
"""

def sleep_progress(seconds):
    m = int(seconds/60)
    s = seconds % 60
    t = [60]*m
    t.append(s)
    assert abs(sum(t) - seconds) < .00001
    for q in tqdm(t):
        time.sleep(q)


def get_secs_until(end, utc):
    if utc:
        current_time = datetime.datetime.utcnow()
    else:
        current_time = datetime.datetime.now()
    delta = end - current_time
    return delta.total_seconds()

class StockAlgorithm():

    """
    Globals
    """
    tickers = []

    def __init__(self, max_amount=None, stop_loss_percent=4):
        # if max_amount = None OR if max_amount >= portfolio value, will use entire portfolio
        self.trader = RobinhoodTrader(stop_loss_percent)
        self.nyse_calendar = mcal.get_calendar("NYSE")
        if max_amount is None:
            max_amount = 9e9
        self.max_amount = max_amount
        self.run()

    """
    Initialization
    """
    def is_trading_day(self, nowstr):
        try:
            day = self.nyse_calendar.schedule(start_date=nowstr, end_date=nowstr)
            return True, day
        except:
            print("{} is not a valid trading day!".format(nowstr))
            return False, None

    def get_market_times(self, day):
        self.market_open = day['market_open'][day.index[0]]
        self.market_close = day['market_close'][day.index[0]]

    def run(self):
        while True:
            now = datetime.datetime.now()
            nowstr = datetime.datetime.now().strftime("%Y-%m-%d")

            print("\t{}".format(nowstr))

            is_trading_day, day = self.is_trading_day(nowstr)

            if is_trading_day:
                self.get_market_times(day)

                time_market_open = get_secs_until(self.market_open, utc=True) + .2
                time_market_close = get_secs_until(self.market_close, utc=True)

                if time_market_open > 0:
                    print("Waiting for market open...")
                    sleep_progress(time_market_open)

                    print("\t\t MO - Selling Stocks")
                    self.trader.sell_off_stocks()

                    time.sleep(5)
                    self.during_market()

                if time_market_open <= 0 and time_market_close > 0:
                    print("Market open...")
                    self.during_market()

                if time_market_close <= 0:
                    print("Market has already closed...")

            tomorrow_morning = datetime.date.today() + datetime.timedelta(days=1)
            dt = datetime.datetime(month=tomorrow_morning.month, day=tomorrow_morning.day, year=tomorrow_morning.year)
            delta = get_secs_until(dt, utc=False) + 10
            print("Wait until tomorrow... sleeping for {} seconds".format(delta))
            sleep_progress(delta)

    """
    Strategy
    """
    def screen_stocks(self):
        self.tickers = Optimizers.obtain_maxalpha_tickers()
        print("Maxalpha tickers:", str(self.tickers))
        x,y = Optimizers.get_premarket_timestamps()

        stock_eval = []
        for ticker in self.tickers:
            if Optimizers.is_tradeable(ticker):
                has_data, stock_data = Optimizers.obtain_data(ticker, x, y)

                if has_data:
                    macd_trend = Optimizers.MACD(stock_data)
                    rsi_trend = Optimizers.RSI(stock_data)

                    if macd_trend + rsi_trend >= 0:
                        stock_eval.append(ticker)
                        print("\t\t\t {} is good!! \t MACD:{} RSI:{}".format(ticker, macd_trend, rsi_trend))
                    else:
                        print("\t\t\t {} is not good \t MACD:{} RSI:{}".format(ticker, macd_trend, rsi_trend))
                else:
                    print("\t\t\t {} doesn't have data".format(ticker))
            else:
                print("\t\t\t {} isn't tradable".format(ticker))

        self.screened_stocks = stock_eval

    def hold_after_hours(self, ticker):
        tomorrow_morning = datetime.date.today() + datetime.timedelta(days=1)
        dt = datetime.datetime(month=tomorrow_morning.month, day=tomorrow_morning.day, year=tomorrow_morning.year)

        tom_str = dt.strftime("%Y-%m-%d")
        try:
            day = self.nyse_calendar.schedule(start_date=tom_str, end_date=tom_str)

            has_data, stock_data = Optimizers.obtain_data(ticker, int(self.market_open.timestamp()), int(self.market_close.timestamp()))
            if has_data:
                macd_trend = Optimizers.MACD(stock_data)
                rsi_trend = Optimizers.RSI(stock_data)

                if macd_trend + rsi_trend >= 0:
                    return True
                return False
            return False
        except Exception as e:
            print(e)
            print("\t\t\t Tomorrow is not trading day, dumping stock...")
            return False

    """
    Daily Breakdown
    """

    def find_stop_loss_perc(self, ticker):
        x,y = Optimizers.get_premarket_timestamps()
        has_data, stock_data = Optimizers.obtain_data(ticker, x,y)
        if has_data:
            return Optimizers.get_stop_loss(stock_data)
        return 4


    def during_market(self):
        self.trader.trader.logout()
        self.trader.login()
        print("\t\t Screening Stocks")
        self.screen_stocks()

        if len(self.screened_stocks) > 0:
            print("\t\t Buying Stock")
            
            self.trader = self.screened_stocks[0]
            self.trader.purchase_stocks([self.screened_stocks[0]], self.max_amount)

        print("\t\t Trading Stocks")
        while datetime.datetime.utcnow() < self.market_close - datetime.timedelta(seconds=30):
            self.trader.day_trade()
            time.sleep(1)

        if not self.hold_after_hours(self.screened_stocks[0]):
            self.trader.sell_off_stocks()

if __name__ == "__main__":
    max_amount = input("Max amount (leave blank for all): ")
    try:
        max_amount = int(max_amount)
    except:
        max_amount = None
    print("{}".format("inf" if max_amount is None else max_amount))
    a = StockAlgorithm(max_amount=max_amount)
