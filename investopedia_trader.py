import requests
import datetime
import time
import threading
import getpass
from InvestopediaApi import ita
from configparser import ConfigParser

class InvestopediaTrader():

    stock_list = []
    stock_orders = []

    def __init__(self, stop_loss_percent=4):
        self.configure()
        self.login()
        del self.trader.session.headers["X-Robinhood-API-Version"]

        # self.day_strategy = day_strategy
        self.stop_loss_percent = stop_loss_percent
        self.max_prices = {}
        self.prices = {}
        self.last_price_time = datetime.datetime.now()
        self.day_trades = []

    def configure(self):
        self.username = input("Username: ")
        self.password = getpass.getpass()

    def login(self):
        print("---------------        login...        ----------------")
        self.trader = ita.Account(self.username, self.password)
        print("---------------        logged in.      ----------------")

        account = self.trader.get_account()
        self.acc_id = account["account_number"]
        self.acc_url = account["url"]

    """
    Helpers
    """
    def current_portfolio(self):
        return {sec.symbol:sec.purchase_price sec in self.trader.get_current_securities()}

    def num_day_trades(self):
        day_trades = self.trader.session.get("https://api.robinhood.com/accounts/{}/recent_day_trades/".format(self.acc_id)).json()
        return len(day_trades["equity_day_trades"]) + len(day_trades["option_day_trades"])

    def get_instrument_id(self):
        pass

    def place_order(self,stock_ticker, quantity, type_order):
        instrument_req = self.trader.session.get("https://api.robinhood.com/instruments/?symbol={}".format(stock_ticker.upper())).json()["results"][0]
        stock_instrument = instrument_req["url"]
        stock_quote = self.trader.session.get("https://api.robinhood.com/quotes/{}/".format(stock_ticker)).json()["bid_price"]
        r = self.trader.session.post("https://api.robinhood.com/orders/",
            data={
            "account":self.acc_url,
            "instrument":stock_instrument,
            "symbol":stock_ticker.upper(),
            "type":"market",
            "time_in_force":"gtc",
            "trigger":"immediate",
            "price":stock_quote,
            "quantity":quantity,
            "side":type_order
            }
        )
        return r.json()["id"]

    """
    Initial
    """
    def purchase_stocks(self, stock_list, max_amount):
        print("Purchasing Stocks")
        day_trades = self.num_day_trades()
        amount = min(max_amount, self.buying_power())
        print("Current buying power: {} Day trades: {}".format(amount, day_trades))
        if day_trades < 3:
            purchase_power = amount*.95
            amount_per_stock = purchase_power/len(stock_list)

            data = self.trader.get_quote_list(",".join(stock_list), 'symbol,last_trade_price')

            for d in data:
                self.max_prices[d[0]] = float(d[1])
                quantity = int(amount_per_stock/float(d[1]))
                # quantity = min(quantity, 1)
                print("Buying ", d[0], quantity, float(d[1]) )
                if quantity > 0:
                    order_id = self.place_order(d[0], quantity, "buy")
                    t = threading.Thread(target=self.verify_order, args=[order_id])
                    t.start()
                    pass
            return True
        else:
            print("TOO MANY DAY TRADES!!!")
        return False

    def buying_power(self):
        return self.trader.get_portfolio_status().buying_power

    """
    During Market
    """

    def verify_order(self, order_id):
        while True:
            time.sleep(20)

            try:
                res = self.trader.session.get("https://api.robinhood.com/orders/{}".format(order_id)).json()
                instrument = res["instrument"]
                # print(instrument)
                ticker = self.trader.session.get(instrument).json()["symbol"]
                quantity = float(res["quantity"])
                type_pur = res["side"]
                state = res["state"]
                cancel = res["cancel"]
                print(ticker, state)

                if type_pur == "buy" and ticker in self.current_portfolio():
                    state == "filled"
                if type_pur == "sell" and ticker not in self.current_portfolio():
                    state == "filled"

                if state == "filled" or state == "cancelled":
                    return
                else:
                    if cancel:
                        print("TRYING AGAIN")
                        self.trader.session.post(cancel)
                    order_id = self.place_order(ticker, quantity, type_pur)
            except Exception as e:
                print(e)

    def sell_off_stocks(self):
        portfolio = self.current_portfolio()
        if len(portfolio.keys()) > 0:
            for ticker, quantity in portfolio.items():
                if quantity > 0:
                    try:
                        order_id = self.place_order(ticker, quantity, "sell")
                        t = threading.Thread(target=self.verify_order, args=[order_id])
                        t.start()
                    except:
                        print("Sell call failed... Check robinhood...")


    def day_trade(self):
        portfolio = self.current_portfolio()
        print("\t\t\t\t", portfolio)
        if len(portfolio.keys()) > 0:
            data = self.trader.get_quote_list(",".join(portfolio.keys()),'symbol,last_trade_price')
            for d in data:
                ticker = d[0]
                price = float(d[1])

                if ticker not in self.max_prices:
                    self.max_prices[ticker] = price
                self.max_prices[ticker] = max(self.max_prices[ticker], price)

            for d in data:
                quantity = portfolio[d[0]]
                max_price = self.max_prices[d[0]]
                price = float(d[1])
                if price < max_price*(1-self.stop_loss_percent*.01):
                    try:
                        order_id = self.place_order(d[0], quantity, "sell")
                        t = threading.Thread(target=self.verify_order, args=[order_id])
                        t.start()
                    except:
                        print("Sell call failed... Check robinhood...")
